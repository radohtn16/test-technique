<?php

namespace App\Service;


use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class UploadFileService
{
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;

    }

    public function uploadFile(UploadedFile $file)
    {
        // get original name of the file
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        // this is needed to safely include the file name as part of the URL
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        // Move the file to the directory where xlsx are stored
        try {
            $file->move(
                __DIR__ . '/../../public/uploads/',  //choose the folder in which the uploaded file will be stored
                $fileName
            );
        } catch (FileException $e) {
            return null;
        }

        return $fileName;
    }
}
