<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Service\UploadFileService;
use App\Service\XlsxService;
use App\Entity\Xlsx;
use App\Form\XlsxType;
use App\Repository\XlsxRepository;
use Doctrine\ORM\EntityManagerInterface;

class ImportDataController extends AbstractController
{

    /**
     * @Route("/", name="app_import_data")
     */
    public function importData(Request $request, UploadFileService $uploadFileService, XlsxService $xlsxService, EntityManagerInterface $em, XlsxRepository $xlsxRepository): Response
    {

        $xlsx = new Xlsx;
        $form = $this->createForm(XlsxType::class, $xlsx);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $brochureFile */
            $file = $form->get('fileName')->getData();

            // upload file
            $fileName = $uploadFileService->uploadFile($file);

            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            // check if file is already imported
            $xlsxBD = $xlsxRepository->findBy(['originalName' => $originalFilename]);
            if ($xlsxBD) {
                $this->addFlash('alert-warning', 'Ce fichier est déjà importé dans la base de données. Veuillez importer un autre svp');
                return $this->redirectToRoute('app_import_data');
            }

            // set $xlsx variable
            $xlsx->setFileName($fileName);
            $xlsx->setOriginalName($originalFilename);

            // read file and get array
            $sheetData = $xlsxService->readFileAndGetArray($fileName);

            // import data in db
            $xlsxService->importDataToDb($sheetData);

            // persist and flush Xlsx
            $em->persist($xlsx);
            $em->flush();

            // add flash if file imported
            $this->addFlash('alert-success', 'Fichier importé avec succès dans la base de données');

            return $this->redirectToRoute('app_import_data');
        }

        return $this->render('import_data/index.html.twig', [
            'controller_name' => 'ImportDataController',
            'form' => $form->createView(),
        ]);
    }
}
