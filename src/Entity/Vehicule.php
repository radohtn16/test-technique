<?php

namespace App\Entity;

use App\Repository\VehiculeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VehiculeRepository::class)
 */
class Vehicule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $compte_affaire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $compte_evenement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $compte_dernier_evenement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero_fiche;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $libelle_civilite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $proprietaire_actuelle_vehicule;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero_nom_voie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complement_adress_1;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $code_postal;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephone_domicile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephone_portable;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephone_job;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_mise_en_circulation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_achat;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_dernier_evenement_vehicule;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $libelle_marque;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $libelle_modele;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $immatriculation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type_prospect;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $kilometrage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $libelle_energie;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vendeur_vn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vendeur_vo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commentaire_facturation_vehicule;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type_vn_vo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numero_dossier_vn_vo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $intermediare_vente_vn;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_evenement_vehicule;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $origine_evenement_vehicile;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompteAffaire(): ?string
    {
        return $this->compte_affaire;
    }

    public function setCompteAffaire(string $compte_affaire): self
    {
        $this->compte_affaire = $compte_affaire;

        return $this;
    }

    public function getCompteEvenement(): ?string
    {
        return $this->compte_evenement;
    }

    public function setCompteEvenement(?string $compte_evenement): self
    {
        $this->compte_evenement = $compte_evenement;

        return $this;
    }

    public function getCompteDernierEvenement(): ?string
    {
        return $this->compte_dernier_evenement;
    }

    public function setCompteDernierEvenement(?string $compte_dernier_evenement): self
    {
        $this->compte_dernier_evenement = $compte_dernier_evenement;

        return $this;
    }

    public function getNumeroFiche(): ?string
    {
        return $this->numero_fiche;
    }

    public function setNumeroFiche(?string $numero_fiche): self
    {
        $this->numero_fiche = $numero_fiche;

        return $this;
    }

    public function getLibelleCivilite(): ?string
    {
        return $this->libelle_civilite;
    }

    public function setLibelleCivilite(?string $libelle_civilite): self
    {
        $this->libelle_civilite = $libelle_civilite;

        return $this;
    }

    public function getProprietaireActuelleVehicule(): ?string
    {
        return $this->proprietaire_actuelle_vehicule;
    }

    public function setProprietaireActuelleVehicule(?string $proprietaire_actuelle_vehicule): self
    {
        $this->proprietaire_actuelle_vehicule = $proprietaire_actuelle_vehicule;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNumeroNomVoie(): ?string
    {
        return $this->numero_nom_voie;
    }

    public function setNumeroNomVoie(?string $numero_nom_voie): self
    {
        $this->numero_nom_voie = $numero_nom_voie;

        return $this;
    }

    public function getComplementAdress1(): ?string
    {
        return $this->complement_adress_1;
    }

    public function setComplementAdress1(?string $complement_adress_1): self
    {
        $this->complement_adress_1 = $complement_adress_1;

        return $this;
    }

    public function getCodePostal(): ?int
    {
        return $this->code_postal;
    }

    public function setCodePostal(?int $code_postal): self
    {
        $this->code_postal = $code_postal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getTelephoneDomicile(): ?string
    {
        return $this->telephone_domicile;
    }

    public function setTelephoneDomicile(?string $telephone_domicile): self
    {
        $this->telephone_domicile = $telephone_domicile;

        return $this;
    }

    public function getTelephonePortable(): ?string
    {
        return $this->telephone_portable;
    }

    public function setTelephonePortable(?string $telephone_portable): self
    {
        $this->telephone_portable = $telephone_portable;

        return $this;
    }

    public function getTelephoneJob(): ?string
    {
        return $this->telephone_job;
    }

    public function setTelephoneJob(?string $telephone_job): self
    {
        $this->telephone_job = $telephone_job;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDateDernierEvenementVehicule(): ?\DateTimeInterface
    {
        return $this->date_dernier_evenement_vehicule;
    }

    public function setDateDernierEvenementVehicule(?\DateTimeInterface $date_dernier_evenement_vehicule): self
    {
        $this->date_dernier_evenement_vehicule = $date_dernier_evenement_vehicule;

        return $this;
    }

    public function getLibelleMarque(): ?string
    {
        return $this->libelle_marque;
    }

    public function setLibelleMarque(?string $libelle_marque): self
    {
        $this->libelle_marque = $libelle_marque;

        return $this;
    }

    public function getLibelleModele(): ?string
    {
        return $this->libelle_modele;
    }

    public function setLibelleModele(?string $libelle_modele): self
    {
        $this->libelle_modele = $libelle_modele;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(?string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getImmatriculation(): ?string
    {
        return $this->immatriculation;
    }

    public function setImmatriculation(?string $immatriculation): self
    {
        $this->immatriculation = $immatriculation;

        return $this;
    }

    public function getTypeProspect(): ?string
    {
        return $this->type_prospect;
    }

    public function setTypeProspect(?string $type_prospect): self
    {
        $this->type_prospect = $type_prospect;

        return $this;
    }

    public function getKilometrage(): ?string
    {
        return $this->kilometrage;
    }

    public function setKilometrage(?string $kilometrage): self
    {
        $this->kilometrage = $kilometrage;

        return $this;
    }

    public function getLibelleEnergie(): ?string
    {
        return $this->libelle_energie;
    }

    public function setLibelleEnergie(?string $libelle_energie): self
    {
        $this->libelle_energie = $libelle_energie;

        return $this;
    }

    public function getVendeurVn(): ?string
    {
        return $this->vendeur_vn;
    }

    public function setVendeurVn(?string $vendeur_vn): self
    {
        $this->vendeur_vn = $vendeur_vn;

        return $this;
    }

    public function getVendeurVo(): ?string
    {
        return $this->vendeur_vo;
    }

    public function setVendeurVo(?string $vendeur_vo): self
    {
        $this->vendeur_vo = $vendeur_vo;

        return $this;
    }

    public function getCommentaireFacturationVehicule(): ?string
    {
        return $this->commentaire_facturation_vehicule;
    }

    public function setCommentaireFacturationVehicule(?string $commentaire_facturation_vehicule): self
    {
        $this->commentaire_facturation_vehicule = $commentaire_facturation_vehicule;

        return $this;
    }

    public function getTypeVnVo(): ?string
    {
        return $this->type_vn_vo;
    }

    public function setTypeVnVo(?string $type_vn_vo): self
    {
        $this->type_vn_vo = $type_vn_vo;

        return $this;
    }

    public function getNumeroDossierVnVo(): ?string
    {
        return $this->numero_dossier_vn_vo;
    }

    public function setNumeroDossierVnVo(?string $numero_dossier_vn_vo): self
    {
        $this->numero_dossier_vn_vo = $numero_dossier_vn_vo;

        return $this;
    }

    public function getIntermediareVenteVn(): ?string
    {
        return $this->intermediare_vente_vn;
    }

    public function setIntermediareVenteVn(?string $intermediare_vente_vn): self
    {
        $this->intermediare_vente_vn = $intermediare_vente_vn;

        return $this;
    }

    public function getDateEvenementVehicule(): ?\DateTimeInterface
    {
        return $this->date_evenement_vehicule;
    }

    public function setDateEvenementVehicule(?\DateTimeInterface $date_evenement_vehicule): self
    {
        $this->date_evenement_vehicule = $date_evenement_vehicule;

        return $this;
    }

    public function getOrigineEvenementVehicile(): ?string
    {
        return $this->origine_evenement_vehicile;
    }

    public function setOrigineEvenementVehicile(?string $origine_evenement_vehicile): self
    {
        $this->origine_evenement_vehicile = $origine_evenement_vehicile;

        return $this;
    }

    public function getDateMiseEnCirculation(): ?\DateTimeInterface
    {
        return $this->date_mise_en_circulation;
    }

    public function setDateMiseEnCirculation(?\DateTimeInterface $date_mise_en_circulation): self
    {
        $this->date_mise_en_circulation = $date_mise_en_circulation;

        return $this;
    }

    public function getDateAchat(): ?\DateTimeInterface
    {
        return $this->date_achat;
    }

    public function setDateAchat(?\DateTimeInterface $date_achat): self
    {
        $this->date_achat = $date_achat;

        return $this;
    }
}
