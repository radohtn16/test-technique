<?php

namespace App\Repository;

use App\Entity\Xlsx;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Xlsx>
 *
 * @method Xlsx|null find($id, $lockMode = null, $lockVersion = null)
 * @method Xlsx|null findOneBy(array $criteria, array $orderBy = null)
 * @method Xlsx[]    findAll()
 * @method Xlsx[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class XlsxRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Xlsx::class);
    }

    public function add(Xlsx $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Xlsx $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


}
