<?php

namespace App\Service;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Entity\Vehicule;
use Doctrine\ORM\EntityManagerInterface;

class XlsxService
{

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function readFileAndGetArray($filePathName)
    {
        //choose the folder in which the uploaded file will be stored
        $fileFolder = __DIR__ . '/../../public/uploads/';

        // read from the excel file 
        $spreadsheet = IOFactory::load($fileFolder . $filePathName); 

        // remove the first file line
        $spreadsheet->getActiveSheet()->removeRow(1);  

        // the read data is turned into an array
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true); 

        return $sheetData;
    }

    public function importDataToDb($sheetData)
    {
        foreach ($sheetData as $row) 
        { 
            $vehicule = new Vehicule;

            $vehicule
                ->setCompteAffaire($row["A"])
                ->setCompteEvenement($row["B"])
                ->setCompteDernierEvenement($row["C"])
                ->setNumeroFiche($row["D"])
                ->setLibelleCivilite($row["E"])
                ->setProprietaireActuelleVehicule($row["F"])
                ->setNom($row["G"])
                ->setPrenom($row["H"])
                ->setNumeroNomVoie($row["I"])
                ->setComplementAdress1($row["J"])
                ->setCodePostal($row["K"])
                ->setVille($row["L"])
                ->setTelephoneDomicile($row["M"])
                ->setTelephonePortable($row["N"])
                ->setTelephoneJob($row["O"])
                ->setEmail($row["P"])
                ->setDateMiseEnCirculation(new \DateTimeImmutable($row["Q"]))
                ->setDateAchat(new \DateTimeImmutable($row["R"]))
                ->setDateDernierEvenementVehicule(new \DateTimeImmutable($row["S"]))
                ->setLibelleMarque($row["T"])
                ->setLibelleModele($row["U"])
                ->setVersion($row["V"])
                ->setVin($row["W"])
                ->setImmatriculation($row["X"])
                ->setTypeProspect($row["Y"])
                ->setKilometrage($row["Z"])
                ->setLibelleEnergie($row["AA"])
                ->setVendeurVn($row["AB"])
                ->setVendeurVo($row["AC"])
                ->setCommentaireFacturationVehicule($row["AD"])
                ->setTypeVnVo($row["AE"])
                ->setNumeroDossierVnVo($row["AF"])
                ->setIntermediareVenteVn($row["AG"])
                ->setDateEvenementVehicule(new \DateTimeImmutable($row["AH"]))
                ->setOrigineEvenementVehicile($row["AI"])
            ;

            $this->em->persist($vehicule);
        }

        $this->em->flush();

    }
}